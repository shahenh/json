
import org.everit.json.schema.Schema;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class JsonSchemaValidation {
    public static void main(String[] args) throws FileNotFoundException {
        File schemaFile = new File("jsonSchema.json");

        JSONTokener schemaData = new JSONTokener(new FileInputStream(schemaFile));
        JSONObject jsonSchema = new JSONObject(schemaData);

        File jsonData = new File("json.json");
        JSONTokener jsonDataFile = new JSONTokener(new FileInputStream(jsonData));
        JSONObject jsonObject = new JSONObject(jsonDataFile);

        Schema schemaValidator = SchemaLoader.load(jsonSchema);
        schemaValidator.validate(jsonObject);

        System.out.println(jsonObject.getString("deploymentUnit"));
        System.out.println(jsonObject.getString("operationId"));
        System.out.println(jsonObject.getString("orderNum"));
    }
}